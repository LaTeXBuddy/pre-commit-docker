ARG PRE_COMMIT=main

FROM alpine:latest AS build

ARG PRE_COMMIT

RUN apk add git
WORKDIR /tmp/pre-commit
RUN git clone --depth 1 --branch $PRE_COMMIT https://github.com/pre-commit/pre-commit.git .

FROM python:alpine

RUN apk add --no-cache --purge git

WORKDIR /tmp/build
COPY --from=build /tmp/pre-commit ./
RUN python -m pip install --no-cache-dir . && rm -rf /tmp/build

WORKDIR /app

CMD ["pre-commit"]
