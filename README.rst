=================
pre-commit-docker
=================

This is a Docker image for running `pre-commit`_. We use it in our GitLab CI pipelines to save time spent initializing the image.

.. _pre-commit: https://pre-commit.com/

Licence
-------

Copyright © 2022 LaTeXBuddy
Licensed under the `Apache License 2.0`_.

.. _Apache License 2.0: https://spdx.org/licenses/Apache-2.0.html

---

This project is hosted on GitLab:
https://gitlab.com/LaTeXBuddy/pre-commit-docker.git
